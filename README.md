# monopipeline

Monorepo for microservice delivery using Gitlab, AWX, and Terraform.

# Get started
* Fork this repository
* Settings -> CI/CD variables
    | VariableName          | Description                                                 |
    |-----------------------|-------------------------------------------------------------|
    | AWS_ACCESS_KEY_ID     | This is the AWS access key.                                 |
    | AWS_SECRET_ACCESS_KEY | This is the AWS secret key.                                 |
    | SSH_PRIVATE_KEY       | SSH Private Key.  Must be saved as the `File` variable.     |
    | TF_HTTP_PASSWORD      | Terraform HTTP Backend.  Password for basic authentication. |
    | TF_HTTP_USERNAME      | Terraform HTTP backend.  Username for basic authentication. |
    | TF_VAR_env_publickey  | Terraform input.  Used for creating key pair.               |
    | TOWER_PASSWORD        | Password to access AWX.                                     |
    | TOWER_USERNAME        | Username to access AWX. Must be `admin`                     |
    | TOWER_VERIFY_SSL      | Require SSL to access AWX. Must be `False`                  |
* Start running pipelines

## Pipeline Notes
This repository takes a GitLab Flow approach to environments.
[Docs](https://docs.gitlab.com/ee/topics/gitlab_flow.html#environment-branches-with-gitlab-flow)

### Initializing an environment
Start a pipeline with variable `BOOTSTRAP` value `yes`.

Unable to automate this until [Issue#35315](https://gitlab.com/gitlab-org/gitlab/-/issues/35315) is addressed.

### Destroying an environment
Start a pipeline with variable `DESTROY` value `yes`.