# Terraform does not support defining backend type via parameters
## https://github.com/hashicorp/terraform/issues/24929
terraform {
  backend "http" {
    # HTTP Gitlab Specific Settings
    ## https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html
    lock_method   = "POST"
    unlock_method = "DELETE"
  }
}