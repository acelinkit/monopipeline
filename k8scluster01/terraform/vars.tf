variable "env_region" {
  default = "us-west-2"
}

variable "env_name" {
  type = string
}

variable "env_publickey" {
  type      = string
  sensitive = true
}