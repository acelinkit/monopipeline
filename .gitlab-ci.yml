stages:
  - bootstrap
  - gatherinfo
  - stage1
  - stage2
  - stage3
  - destroy

variables:
  # Ansible Configurations
  ## https://docs.ansible.com/ansible/latest/reference_appendices/config.html
  ANSIBLE_STDOUT_CALLBACK: "yaml"
  ANSIBLE_LOCALHOST_WARNING: "False"

create_environment:
  stage: bootstrap
  image:
    name: "docker.io/acelinkit/ansibletools:0.4.0"
  script:
    - |
        ### SET GENERAL VARIABLES BLOCK ###
        # TERRAFROM BACKEND HTTP CONFIGS
        ## https://www.terraform.io/docs/language/settings/backends/http.html#configuration-variables
        export TF_HTTP_USERNAME=$TF_HTTP_USERNAME
        export TF_HTTP_PASSWORD=$TF_HTTP_PASSWORD

        # TERRAFORM INPUT VARIABLES
        ## https://www.terraform.io/docs/language/values/variables.html#environment-variables
        export TF_VAR_env_publickey=$TF_VAR_env_publickey
        export TF_VAR_env_name=$CI_COMMIT_REF_SLUG

        # AWS PROVIDER CONFIGS
        ## https://registry.terraform.io/providers/hashicorp/aws/latest/docs#environment-variables
        export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
        export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

        # ANSIBLE CONFIGS
        export ANSIBLE_REMOTE_USER=ubuntu
        export ANSIBLE_INVENTORY=$CI_PROJECT_DIR/k8scluster01/artifact/localinventory.ini
        export K8S_AUTH_KUBECONFIG=$CI_PROJECT_DIR/k8scluster01/artifact/config

        export TOWER_USERNAME=$TOWER_USERNAME
        export TOWER_PASSWORD=$TOWER_PASSWORD
        export TOWER_VERIFY_SSL=$TOWER_VERIFY_SSL

        # SSH PRIVATEKEY LOCATION
        export SSH_PRIVATEKEY_FILE="$CI_PROJECT_DIR/id_rsa"
        cp $SSH_PRIVATE_KEY $SSH_PRIVATEKEY_FILE
        chmod 0600 $SSH_PRIVATEKEY_FILE
        export ANSIBLE_PRIVATE_KEY_FILE="$SSH_PRIVATEKEY_FILE"

    - ansible-galaxy install -r $CI_PROJECT_DIR/roles/requirements.yaml
    - ansible-galaxy collection install -r $CI_PROJECT_DIR/collections/requirements.yaml
    # awsvpc
    - |
        export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$CI_COMMIT_REF_SLUG-awsvpc"
        export TF_HTTP_LOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
        export TF_HTTP_UNLOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
    - ansible-playbook awsvpc/playbook.yaml
    # k8scluster01
    - |
        export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$CI_COMMIT_REF_SLUG-k8scluster01"
        export TF_HTTP_LOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
        export TF_HTTP_UNLOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
    - ansible-playbook k8scluster01/terraform.yaml
    - export TOWER_HOST=$(cat $CI_PROJECT_DIR/k8scluster01/artifact/towerhost)
    - echo "TOWER_HOST=$TOWER_HOST" >> variable.env
    - ansible-playbook k8scluster01/playbook.yaml
    # AWX
    ## SETTING TF_HTTP_ADDRESS BASE URL
    - export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$CI_COMMIT_REF_SLUG"
    - ansible-playbook awx/playbook.yaml
  environment:
    name: $CI_COMMIT_REF_SLUG
    url: $TOWER_HOST
  artifacts:
    reports:
      dotenv: variable.env
  rules:
    # Cannot use environment to determine if bootstrap is needed
    ## https://gitlab.com/gitlab-org/gitlab/-/issues/35315
    - if: $BOOTSTRAP == "yes"

# New Job because CI_ENVIRONMENT_URL cannot be used in same environment its declared
## https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#gitlab-ciyml-file
get_awx:
  stage: gatherinfo
  script:
    - |
      if [[ -z $CI_ENVIRONMENT_URL ]]; then
        echo ERROR: No environment URL found.
        exit 1
      fi
    - echo Set TOWER_HOST so it can be used by other jobs
    - echo $CI_ENVIRONMENT_URL
    - echo "TOWER_HOST=$CI_ENVIRONMENT_URL" >> variable.env
  environment:
    name: $CI_COMMIT_REF_SLUG
  artifacts:
    reports:
      dotenv: variable.env

awsvpc:
  stage: stage1
  image:
    name: "docker.io/acelinkit/ansibletools:0.4.0"
  script:
    - ansible-playbook awsvpc/awx.yaml

k8scluster01:
  stage: stage2
  image:
    name: "docker.io/acelinkit/ansibletools:0.4.0"
  script:
    - ansible-playbook k8scluster01/awx.yaml

awx:
  stage: stage3
  image:
    name: "docker.io/acelinkit/ansibletools:0.4.0"
  script:
    - ansible-playbook awx/awx.yaml

destroy_environment:
  stage: destroy
  image:
    name: "docker.io/acelinkit/ansibletools:0.4.0"
  script:
    - |
        # TERRAFROM BACKEND HTTP CONFIGS
        ## https://www.terraform.io/docs/language/settings/backends/http.html#configuration-variables
        export TF_HTTP_USERNAME=$TF_HTTP_USERNAME
        export TF_HTTP_PASSWORD=$TF_HTTP_PASSWORD

        # TERRAFORM INPUT VARIABLES
        ## https://www.terraform.io/docs/language/values/variables.html#environment-variables
        export TF_VAR_env_publickey=$TF_VAR_env_publickey
        export TF_VAR_env_name=$CI_COMMIT_REF_SLUG

        # AWS PROVIDER CONFIGS
        ## https://registry.terraform.io/providers/hashicorp/aws/latest/docs#environment-variables
        export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
        export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    - |
        export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$CI_COMMIT_REF_SLUG-k8scluster01"
        export TF_HTTP_LOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
        export TF_HTTP_UNLOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
    - ansible-playbook k8scluster01/terraform.yaml --extra-vars "state=absent"
    - |
        export TF_HTTP_ADDRESS="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$CI_COMMIT_REF_SLUG-awsvpc"
        export TF_HTTP_LOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
        export TF_HTTP_UNLOCK_ADDRESS="$TF_HTTP_ADDRESS/lock"
    - ansible-playbook awsvpc/playbook.yaml --extra-vars "state=absent"
  environment:
    name: $CI_COMMIT_REF_SLUG
    action: stop
  rules:
    - if: $DESTROY == "yes"