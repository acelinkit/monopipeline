resource "aws_internet_gateway" "igw_0" {
  vpc_id = aws_vpc.vpc_0.id
}

resource "aws_route_table" "crt_0" {
  vpc_id = aws_vpc.vpc_0.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_0.id
  }
}

resource "aws_route_table_association" "rta_0" {
  subnet_id      = aws_subnet.subnet_0.id
  route_table_id = aws_route_table.crt_0.id
}

resource "aws_security_group" "sg_0" {
  vpc_id = aws_vpc.vpc_0.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Kubernetes Api Endpoint
  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # AWX Endpoint via nodeport
  ingress {
    from_port   = 32000
    to_port     = 32000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    env_name_group = var.env_name
    function = "generalsg"
  }
}